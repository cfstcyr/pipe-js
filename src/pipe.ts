/* eslint-disable @typescript-eslint/no-explicit-any */
import { PipeOperator } from './pipe-operator';

function pipe<A>(input: A): A;
function pipe<A, B>(input: A, f1: PipeOperator<A, B>): B;
function pipe<A, B, C>(input: A, f1: PipeOperator<A, B>, f2: PipeOperator<B, C>): C;
function pipe<A, B, C, D>(input: A, f1: PipeOperator<A, B>, f2: PipeOperator<B, C>, f3: PipeOperator<C, D>): D;
function pipe<A, B, C, D, E>(input: A, f1: PipeOperator<A, B>, f2: PipeOperator<B, C>, f3: PipeOperator<C, D>, f4: PipeOperator<D, E>): E;
function pipe<A, B, C, D, E, F>(
  input: A,
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
): F;
function pipe<A, B, C, D, E, F, G>(
  input: A,
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
  f6: PipeOperator<F, G>,
): G;
function pipe<A, B, C, D, E, F, G, H>(
  input: A,
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
  f6: PipeOperator<F, G>,
  f7: PipeOperator<G, H>,
): H;
function pipe<A, B, C, D, E, F, G, H, I>(
  input: A,
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
  f6: PipeOperator<F, G>,
  f7: PipeOperator<G, H>,
  f8: PipeOperator<H, I>,
): I;
function pipe<A, B, C, D, E, F, G, H, I, J>(
  input: A,
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
  f6: PipeOperator<F, G>,
  f7: PipeOperator<G, H>,
  f8: PipeOperator<H, I>,
  f9: PipeOperator<I, J>,
): J;
function pipe<A, B, C, D, E, F, G, H, I, J>(
  input: A,
  f1?: PipeOperator<A, B>,
  f2?: PipeOperator<B, C>,
  f3?: PipeOperator<C, D>,
  f4?: PipeOperator<D, E>,
  f5?: PipeOperator<E, F>,
  f6?: PipeOperator<F, G>,
  f7?: PipeOperator<G, H>,
  f8?: PipeOperator<H, I>,
  f9?: PipeOperator<I, J>,
): A | B | C | D | E | F | G | H | I | J {
  type all = A | B | C | D | E | F | G | H | I | J;
  type allF = PipeOperator<any, all> | undefined;

  const functions: allF[] = [f1, f2, f3, f4, f5, f6, f7, f8, f9];

  return functions.reduce((o: all, f) => (f ? f(o) : o), input);
}

export default pipe;
