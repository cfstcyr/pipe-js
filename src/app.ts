import { createPipe, pipe } from '.';
import { copy, filter, forEach, join, map, randomItem, sortBy } from './operators/array';
import { greaterOrEqual, ifTrue, lowerOrEqual } from './operators/logic';
import { sum, sumWith } from './operators/math';
import { attributes, set } from './operators/object';
import { charAt, charCodeAt, getLength, indexOf, lastIndexOf, lowerCase, match, replace, search, slice, split } from './operators/string';

type RestaurantItem = { name: string; price: number };
type Wallet = { bills: number[]; coins: number[] };

const menu: RestaurantItem[] = [
  { name: 'hamburger', price: 8.99 },
  { name: 'fries', price: 3.49 },
  { name: 'soda', price: 1.99 },
];

const wallet: Wallet = {
  bills: [5, 5],
  coins: [0.25, 1, 0.1, 0.1, 1, 1, 0.25, 0.25, 1, 0.05],
};

const moneyInWallet = pipe(wallet, attributes('bills', 'coins'), map(sum()), sum());

const order = pipe(
  menu,
  sumWith((item) => item.price),
  lowerOrEqual(moneyInWallet),
  ifTrue('Yay!  I can buy it.', "I can't buy it :("),
);

let moneyInWalletLoop = 0;
for (const bill of wallet.bills) {
  moneyInWalletLoop += bill;
}
for (const coin of wallet.coins) {
  moneyInWalletLoop += coin;
}

let orderTotal = 0;
for (const item of menu) {
  orderTotal += item.price;
}

let result;
if (orderTotal <= moneyInWalletLoop) {
  result = 'Yay!  I can buy it.';
} else {
  result = "I can't buy it :(";
}

console.log(order);
