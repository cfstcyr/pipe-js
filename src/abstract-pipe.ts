/* eslint-disable @typescript-eslint/no-explicit-any */
import { PipeOperator } from './pipe-operator';

abstract class AbstractPipe<T = any> {
  pipe(): this;
  pipe<A extends T>(f1: PipeOperator<this, A>): A;
  pipe<A extends T, B extends T>(f1: PipeOperator<this, A>, f2: PipeOperator<A, B>): B;
  pipe<A extends T, B extends T, C extends T>(f1: PipeOperator<this, A>, f2: PipeOperator<A, B>, f3: PipeOperator<B, C>): C;
  pipe<A extends T, B extends T, C extends T, D extends T>(
    f1: PipeOperator<this, A>,
    f2: PipeOperator<A, B>,
    f3: PipeOperator<B, C>,
    f4: PipeOperator<C, D>,
  ): D;
  pipe<A extends T, B extends T, C extends T, D extends T, E extends T>(
    f1: PipeOperator<this, A>,
    f2: PipeOperator<A, B>,
    f3: PipeOperator<B, C>,
    f4: PipeOperator<C, D>,
    f5: PipeOperator<D, E>,
  ): E;
  pipe<A extends T, B extends T, C extends T, D extends T, E extends T, F extends T>(
    f1: PipeOperator<this, A>,
    f2: PipeOperator<A, B>,
    f3: PipeOperator<B, C>,
    f4: PipeOperator<C, D>,
    f5: PipeOperator<D, E>,
    f6: PipeOperator<E, F>,
  ): F;
  pipe<A extends T, B extends T, C extends T, D extends T, E extends T, F extends T, G extends T>(
    f1: PipeOperator<this, A>,
    f2: PipeOperator<A, B>,
    f3: PipeOperator<B, C>,
    f4: PipeOperator<C, D>,
    f5: PipeOperator<D, E>,
    f6: PipeOperator<E, F>,
    f7: PipeOperator<F, G>,
  ): G;
  pipe<A extends T, B extends T, C extends T, D extends T, E extends T, F extends T, G extends T, H extends T>(
    f1: PipeOperator<this, A>,
    f2: PipeOperator<A, B>,
    f3: PipeOperator<B, C>,
    f4: PipeOperator<C, D>,
    f5: PipeOperator<D, E>,
    f6: PipeOperator<E, F>,
    f7: PipeOperator<F, G>,
    f8: PipeOperator<G, H>,
  ): H;
  pipe<A extends T, B extends T, C extends T, D extends T, E extends T, F extends T, G extends T, H extends T, I extends T>(
    f1: PipeOperator<this, A>,
    f2: PipeOperator<A, B>,
    f3: PipeOperator<B, C>,
    f4: PipeOperator<C, D>,
    f5: PipeOperator<D, E>,
    f6: PipeOperator<E, F>,
    f7: PipeOperator<F, G>,
    f8: PipeOperator<G, H>,
    f9: PipeOperator<H, I>,
  ): I;
  pipe<A extends T, B extends T, C extends T, D extends T, E extends T, F extends T, G extends T, H extends T, I extends T, J extends T>(
    f1: PipeOperator<this, A>,
    f2: PipeOperator<A, B>,
    f3: PipeOperator<B, C>,
    f4: PipeOperator<C, D>,
    f5: PipeOperator<D, E>,
    f6: PipeOperator<E, F>,
    f7: PipeOperator<F, G>,
    f8: PipeOperator<G, H>,
    f9: PipeOperator<H, I>,
    f10: PipeOperator<I, J>,
  ): J;
  pipe<A extends T, B extends T, C extends T, D extends T, E extends T, F extends T, G extends T, H extends T, I extends T, J extends T>(
    f1?: PipeOperator<this, A>,
    f2?: PipeOperator<A, B>,
    f3?: PipeOperator<B, C>,
    f4?: PipeOperator<C, D>,
    f5?: PipeOperator<D, E>,
    f6?: PipeOperator<E, F>,
    f7?: PipeOperator<F, G>,
    f8?: PipeOperator<G, H>,
    f9?: PipeOperator<H, I>,
    f10?: PipeOperator<I, J>,
  ): this | A | B | C | D | E | F | G | H | I | J {
    type all = this | A | B | C | D | E | F | G | H | I | J;
    type allF = PipeOperator<any, all> | undefined;

    const functions: allF[] = [f1, f2, f3, f4, f5, f6, f7, f8, f9, f10];

    return functions.reduce((o: all, f) => (f ? f(o) : o), this as all);
  }
}

export default AbstractPipe;
