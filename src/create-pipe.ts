/* eslint-disable @typescript-eslint/no-explicit-any */
import { pipe } from '.';
import { PipeOperator } from './pipe-operator';

function createPipe<A>(): (input: A) => A;
function createPipe<A, B>(f1: PipeOperator<A, B>): (input: A) => B;
function createPipe<A, B, C>(f1: PipeOperator<A, B>, f2: PipeOperator<B, C>): (input: A) => C;
function createPipe<A, B, C, D>(f1: PipeOperator<A, B>, f2: PipeOperator<B, C>, f3: PipeOperator<C, D>): (input: A) => D;
function createPipe<A, B, C, D, E>(f1: PipeOperator<A, B>, f2: PipeOperator<B, C>, f3: PipeOperator<C, D>, f4: PipeOperator<D, E>): (input: A) => E;
function createPipe<A, B, C, D, E, F>(
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
): (input: A) => F;
function createPipe<A, B, C, D, E, F, G>(
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
  f6: PipeOperator<F, G>,
): (input: A) => G;
function createPipe<A, B, C, D, E, F, G, H>(
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
  f6: PipeOperator<F, G>,
  f7: PipeOperator<G, H>,
): (input: A) => H;
function createPipe<A, B, C, D, E, F, G, H, I>(
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
  f6: PipeOperator<F, G>,
  f7: PipeOperator<G, H>,
  f8: PipeOperator<H, I>,
): (input: A) => I;
function createPipe<A, B, C, D, E, F, G, H, I, J>(
  f1: PipeOperator<A, B>,
  f2: PipeOperator<B, C>,
  f3: PipeOperator<C, D>,
  f4: PipeOperator<D, E>,
  f5: PipeOperator<E, F>,
  f6: PipeOperator<F, G>,
  f7: PipeOperator<G, H>,
  f8: PipeOperator<H, I>,
  f9: PipeOperator<I, J>,
): (input: A) => J;
function createPipe<A, B, C, D, E, F, G, H, I, J>(
  f1?: PipeOperator<A, B>,
  f2?: PipeOperator<B, C>,
  f3?: PipeOperator<C, D>,
  f4?: PipeOperator<D, E>,
  f5?: PipeOperator<E, F>,
  f6?: PipeOperator<F, G>,
  f7?: PipeOperator<G, H>,
  f8?: PipeOperator<H, I>,
  f9?: PipeOperator<I, J>,
): (input: A) => A | B | C | D | E | F | G | H | I | J {
  if (f1 && f2 && f3 && f4 && f5 && f6 && f7 && f8 && f9) return (input: A) => pipe(input, f1, f2, f3, f4, f5, f6, f7, f8, f9);
  if (f1 && f2 && f3 && f4 && f5 && f6 && f7 && f8) return (input: A) => pipe(input, f1, f2, f3, f4, f5, f6, f7, f8);
  if (f1 && f2 && f3 && f4 && f5 && f6 && f7) return (input: A) => pipe(input, f1, f2, f3, f4, f5, f6, f7);
  if (f1 && f2 && f3 && f4 && f5 && f6) return (input: A) => pipe(input, f1, f2, f3, f4, f5, f6);
  if (f1 && f2 && f3 && f4 && f5) return (input: A) => pipe(input, f1, f2, f3, f4, f5);
  if (f1 && f2 && f3 && f4) return (input: A) => pipe(input, f1, f2, f3, f4);
  if (f1 && f2 && f3) return (input: A) => pipe(input, f1, f2, f3);
  if (f1 && f2) return (input: A) => pipe(input, f1, f2);
  if (f1) return (input: A) => pipe(input, f1);
  return (input: A) => pipe(input);
}

export default createPipe;
