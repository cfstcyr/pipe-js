export type PipeOperator<Input, Output = Input> = (input: Input) => Output;
