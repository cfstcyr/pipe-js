import { PipeOperator } from '../pipe-operator';

export const equals =
  <T>(other: T): PipeOperator<T, boolean> =>
  (input: T) =>
    input === other;

export const greater =
  <T>(other: T): PipeOperator<T, boolean> =>
  (input: T) =>
    input > other;

export const lower =
  <T>(other: T): PipeOperator<T, boolean> =>
  (input: T) =>
    input < other;

export const greaterOrEqual =
  <T>(other: T): PipeOperator<T, boolean> =>
  (input: T) =>
    input >= other;

export const lowerOrEqual =
  <T>(other: T): PipeOperator<T, boolean> =>
  (input: T) =>
    input <= other;

export const ifTrue =
  <T>(trueValue: T, elseValue: T): PipeOperator<boolean, T> =>
  (input: boolean) =>
    input ? trueValue : elseValue;

export const ifFalse =
  <T>(falseValue: T, elseValue: T): PipeOperator<boolean, T> =>
  (input: boolean) =>
    !input ? falseValue : elseValue;
