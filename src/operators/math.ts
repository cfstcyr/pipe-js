import { PipeOperator } from '../pipe-operator';

export const add =
  (value: number): PipeOperator<number> =>
  (input: number) =>
    input + value;

export const subtract =
  (value: number): PipeOperator<number> =>
  (input: number) =>
    input - value;

export const multiply =
  (value: number): PipeOperator<number> =>
  (input: number) =>
    input * value;

export const divide =
  (value: number): PipeOperator<number> =>
  (input: number) =>
    input / value;

export const ceil = (): PipeOperator<number> => (input: number) => Math.ceil(input);

export const floor = (): PipeOperator<number> => (input: number) => Math.floor(input);

export const max = (): PipeOperator<number[], number> => (input: number[]) => Math.max(...input);

export const maxWith =
  <T>(predicate: (i: T) => number): PipeOperator<T[], number> =>
  (input: T[]) =>
    Math.max(...input.map(predicate));

export const min = (): PipeOperator<number[], number> => (input: number[]) => Math.min(...input);

export const minWith =
  <T>(predicate: (i: T) => number): PipeOperator<T[], number> =>
  (input: T[]) =>
    Math.min(...input.map(predicate));

export const sum = (): PipeOperator<number[], number> => (input: number[]) => input.reduce((total, val) => total + val, 0);

export const sumWith =
  <T>(predicate: (item: T) => number): PipeOperator<T[], number> =>
  (input: T[]) =>
    input.reduce((total, val) => total + predicate(val), 0);
