import { PipeOperator } from '../pipe-operator';

export const attribute =
  <T, S extends keyof T = keyof T>(key: S): PipeOperator<T, T[S]> =>
  (input: T) =>
    input[key];

export const attributes =
  <T, S extends keyof T = keyof T>(...keys: S[]): PipeOperator<T, T[S][]> =>
  (input: T) =>
    keys.map((key) => input[key]);

export const set =
  <T, S extends keyof T = keyof T, U extends T[S] = T[S]>(key: S, value: U): PipeOperator<T> =>
  (input: T) => {
    input[key] = value;
    return input;
  };
