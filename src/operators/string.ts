import { PipeOperator } from '../pipe-operator';

export const pad =
  (length: number, char: string): PipeOperator<string, string> =>
  (input: string) =>
    `${input}${new Array(length - input.length).fill(char).join('')}`;

export const padStart =
  (length: number, char: string): PipeOperator<string, string> =>
  (input: string) =>
    `${new Array(length - input.length).fill(char).join('')}${input}`;

export const concat =
  (...strings: string[]): PipeOperator<string, string> =>
  (input: string) =>
    input.concat(...strings);

export const charAt =
  (pos: number): PipeOperator<string, string> =>
  (input: string) =>
    input.charAt(pos);

export const charCodeAt =
  (index: number): PipeOperator<string, number> =>
  (input: string) =>
    input.charCodeAt(index);

export const indexOf =
  (searchValue: string, position?: number): PipeOperator<string, number> =>
  (input: string) =>
    input.indexOf(searchValue, position);

export const lastIndexOf =
  (searchValue: string, position?: number): PipeOperator<string, number> =>
  (input: string) =>
    input.lastIndexOf(searchValue, position);

export const match =
  (regexp: string | RegExp): PipeOperator<string, RegExpMatchArray | null> =>
  (input: string) =>
    input.match(regexp);

export const replace =
  (searchValue: string | RegExp, replaceValue: string): PipeOperator<string, string> =>
  (input: string) =>
    input.replace(searchValue, replaceValue);

export const search =
  (regexp: string | RegExp): PipeOperator<string, number> =>
  (input: string) =>
    input.search(regexp);

export const slice =
  (start?: number, end?: number): PipeOperator<string, string> =>
  (input: string) =>
    input.slice(start, end);

export const split =
  (separator: string | RegExp, limit?: number): PipeOperator<string, string[]> =>
  (input: string) =>
    input.split(separator, limit);

export const lowerCase = (): PipeOperator<string, string> => (input: string) => input.toLowerCase();

export const firstLowerCase = (): PipeOperator<string, string> => (input: string) => input[0].toLowerCase() + input.slice(1);

export const upperCase = (): PipeOperator<string, string> => (input: string) => input.toUpperCase();

export const firstUpperCase = (): PipeOperator<string, string> => (input: string) => input[0].toUpperCase() + input.slice(1);

export const trim = (): PipeOperator<string, string> => (input: string) => input.trim();

export const getLength = (): PipeOperator<{ length: number }, number> => (input) => input.length;
