import { PipeOperator } from '../pipe-operator';

type Vector = { x: number; y: number; z?: number };
type VectorOpt = Partial<Vector>;

export const moveVec =
  (movement: VectorOpt): PipeOperator<Vector> =>
  (input: Vector) => {
    if (movement.x) input.x += movement.x;
    if (movement.y) input.y += movement.y;
    if (movement.z && input.z) input.z += movement.z;
    return input;
  };
