import { PipeOperator } from '../pipe-operator';

export const copy =
  <T>(amount: number): PipeOperator<T, T[]> =>
  (input: T) =>
    new Array(amount).fill(input);

export const forEach =
  <T>(predicate: (item: T, index: number, array: T[]) => void): PipeOperator<T[], T[]> =>
  (input: T[]) => {
    input.forEach(predicate);
    return input;
  };

export const map =
  <T, S = T>(predicate: (item: T, index: number, array: T[]) => S): PipeOperator<T[], S[]> =>
  (input: T[]) =>
    input.map<S>(predicate);

export const sortBy =
  <T extends object>(key: keyof T, order: 'asc' | 'des' = 'asc'): PipeOperator<T[], T[]> =>
  (input: T[]) =>
    input.sort((a, b) => (a[key] > b[key] ? (order === 'des' ? -1 : 1) : order == 'des' ? 1 : -1));

export const filter =
  <T>(predicate: (item: T, index: number, array: T[]) => boolean): PipeOperator<T[], T[]> =>
  (input: T[]) =>
    input.filter(predicate);

export const first =
  <T>(predicate?: (item: T, index: number, array: T[]) => boolean): PipeOperator<T[], T> =>
  (input: T[]) =>
    input.filter(predicate ? predicate : () => true)[0];

export const firstOrDefault =
  <T>(predicate: (item: T, index: number, array: T[]) => boolean): PipeOperator<T[], T | undefined> =>
  (input: T[]) => {
    const filter = input.filter(predicate);
    if (filter.length > 0) return filter[0];
    return undefined;
  };

export const pop =
  <T>(): PipeOperator<T[], T | undefined> =>
  (input: T[]) =>
    input.pop();

export const push =
  <T>(item: T): PipeOperator<T[]> =>
  (input: T[]) => {
    input.push(item);
    return input;
  };

export const concat =
  <T>(...items: T[]): PipeOperator<T[]> =>
  (input: T[]) =>
    input.concat(items);

export const join =
  <T>(separator?: string): PipeOperator<T[], string> =>
  (input: T[]) =>
    input.join(separator);

export const reverse =
  <T>(): PipeOperator<T[]> =>
  (input: T[]) =>
    input.reverse();

export const shift =
  <T>(): PipeOperator<T[], T | undefined> =>
  (input: T[]) =>
    input.shift();

export const slice =
  <T>(start?: number, end?: number): PipeOperator<T[]> =>
  (input: T[]) =>
    input.slice(start, end);

export const sort =
  <T>(compare?: (a: T, b: T) => number): PipeOperator<T[]> =>
  (input: T[]) =>
    input.sort(compare);

export const splice =
  <T>(start: number, deleteCount?: number): PipeOperator<T[]> =>
  (input: T[]) =>
    input.splice(start, deleteCount);

export const unshift =
  <T>(...items: T[]): PipeOperator<T[]> =>
  (input: T[]) => {
    input.unshift(...items);
    return input;
  };

export const indexOf =
  <T>(item: T, fromIndex?: number): PipeOperator<T[], number> =>
  (input: T[]) =>
    input.indexOf(item, fromIndex);

export const lastIndexOf =
  <T>(item: T, fromIndex?: number): PipeOperator<T[], number> =>
  (input: T[]) =>
    input.lastIndexOf(item, fromIndex);

export const every =
  <T>(predicate: (item: T, index: number, array: T[]) => unknown): PipeOperator<T[], boolean> =>
  (input: T[]) =>
    input.every(predicate);

export const some =
  <T>(predicate: (item: T, index: number, array: T[]) => unknown): PipeOperator<T[], boolean> =>
  (input: T[]) =>
    input.some(predicate);

export const reduce =
  <T>(callback: (previous: T, current: T, index: number, array: T[]) => T): PipeOperator<T[], T> =>
  (input: T[]) => {
    return input.reduce(callback);
  };

export const reduceFrom =
  <T, U>(callback: (previous: U, current: T, index: number, array: T[]) => U, initialValue: U): PipeOperator<T[], U> =>
  (input: T[]) => {
    return input.reduce(callback, initialValue);
  };

export const reduceRight =
  <T>(callback: (previous: T, current: T, index: number, array: T[]) => T): PipeOperator<T[], T> =>
  (input: T[]) => {
    return input.reduceRight(callback);
  };

export const reduceRightFrom =
  <T, U>(callback: (previous: U, current: T, index: number, array: T[]) => U, initialValue: U): PipeOperator<T[], U> =>
  (input: T[]) => {
    return input.reduceRight(callback, initialValue);
  };

export const randomItem =
  <T>(): PipeOperator<T[], T> =>
  (input: T[]) => {
    return input[Math.floor(Math.random() * input.length)];
  };
