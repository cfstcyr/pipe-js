# pipe-js
A package for functional programming using pipes.
This package was created and intended to be used with Typescript but works well in Javascript also.

- [installation](#installation)
- [import](#import)
  - [pipe](#pipe)
  - [operators](#operators)
- [usage](#usage)
  - [pipe](#pipe-1)
    - [as a function](#as-a-function)
    - [as a class](#as-a-class)
    - [as a creator](#as-a-creator)
  - [operators library](#operators-library)
    - [↪ string](https://gitlab.com/cfstcyr/pipe-js/-/wikis/Operators-library/String)
    - [↪ array](https://gitlab.com/cfstcyr/pipe-js/-/wikis/operators-library/Array)
  - [custom operators](#custom-operators)

For more documentation, see the [↪ wiki](https://gitlab.com/cfstcyr/pipe-js/-/wikis).

## installation
```console
npm i @cfstcyr/pipe-js
```

## import

### pipe
```typescript
const { pipe, createPipe, AbstractPipe } = require('@cfstcyr/pipe-js');
// Or
import { pipe, createPipe, AbstractPipe } from '@cfstcyr/pipe-js');
```
### operators
```typescript
const { add, subtract } from "@cfstcyr/pipe-js/lib/operators/math";
// Or
const { PipeMath } from "@cfstcyr/pipe-js/lib/operators";
```

## usage

### pipe

#### as a function
```typescript
const result = pipe(
  42, 
  add(10), 
  subtract(5),
); // returns 47
```

#### as a class
```typescript
class Person extends AbstractPipe {
  constructor(
      public firstName: string, 
      public lastName: string, 
      public age: number
    ) { super() }
}

const person = new Person('John', 'Doe', 32);

const result = person.pipe(
  attributes('firstName', 'lastName'), 
  join(' '),
); // returns "John Doe"
```

#### as a creator
```typescript
const fahrenheitToCelsiusPipe = createPipe(
  subtract(32), 
  multiply(5/9),
);

const result1 = fahrenheitToCelsiusPipe(68); // returns 20
const result2 = fahrenheitToCelsiusPipe(59); // returns 15
```

### operators library

See [↪ wiki](https://gitlab.com/cfstcyr/pipe-js/-/wikis/operators-library) for more information.

### custom operators

The `PipeOperator` type allows to create new operators, it is the type that defines every operator in the default library.
The type is optional, but strongly recommended.
| type | definition |
| ---- | ---------- |
| `PipeOperator<Input, Output>`| Defines an operator.<br>`Input`: input type<br>`Output`: output type (default: `Output = Input`) |

```typescript
// With PipeOperator (recommended)
const myOperator = 
  (): PipeOperator<number, string> =>
  (input: number): string => {
    // implementation
  };

// or

// Without PipeOperator
const myOperator =
  (): => (input: number): string => {
    // implementation
  };

pipe(42, myOperator());
```